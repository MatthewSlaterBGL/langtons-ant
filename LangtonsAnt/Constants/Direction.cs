﻿namespace LangtonsAnt.Constants
{
    public enum Direction
    {
        North,
        East,
        South,
        West
    }
}
