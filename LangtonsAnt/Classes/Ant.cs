﻿using System;
using System.Drawing;
using LangtonsAnt.Constants;

namespace LangtonsAnt.Classes
{
    public class Ant
    {
        public Ant(Direction startDirection)
        {
            CurrentDirection = startDirection;
        }

        public Point Location { get; private set; } = new Point(0, 0);
        public Direction CurrentDirection { get; private set; }
      
        public void TurnClockwise()
        {
            switch (CurrentDirection)
            {
                case Direction.North:
                    CurrentDirection = Direction.East;
                    break;
                case Direction.East:
                    CurrentDirection = Direction.South;
                    break;
                case Direction.South:
                    CurrentDirection = Direction.West;
                    break;
                case Direction.West:
                    CurrentDirection = Direction.North;
                    break;
            }
        }

        public void TurnAntiClockwise()
        {
            switch (CurrentDirection)
            {
                case Direction.North:
                    CurrentDirection = Direction.West;
                    break;
                case Direction.East:
                    CurrentDirection = Direction.North;
                    break;
                case Direction.South:
                    CurrentDirection = Direction.East;
                    break;
                case Direction.West:
                    CurrentDirection = Direction.South;
                    break;
            }
        }

        public void Move()
        {
            switch (CurrentDirection)
            {
                case Direction.North:
                    Location = new Point(Location.X, Location.Y + 1);
                    break;
                case Direction.East:
                    Location = new Point(Location.X + 1, Location.Y);
                    break;
                case Direction.South:
                    Location = new Point(Location.X, Location.Y - 1);
                    break;
                case Direction.West:
                    Location = new Point(Location.X - 1, Location.Y);
                    break;
            }
        }
    }
}
