﻿using System.Drawing;

namespace LangtonsAnt.Classes
{
    public class Square
    {
        public Point Location { get; set; }
        public bool IsWhite { get; set; }
    }
}