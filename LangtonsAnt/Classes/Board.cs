﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LangtonsAnt.Classes
{
    public class Board
    {
        public Board()
        {
            for (var i = 0; i < 10; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                 Squares.Add(new Square
                 {
                     Location = new Point(i,j),
                     IsWhite = true
                 });
                }
            }
        }

        public List<Square> Squares { get; set; } = new List<Square>();

        public void ToggleSquare(Point point)
        {
            var matchedSquare = Squares.Single(square => square.Location == point);

            matchedSquare.IsWhite = !matchedSquare.IsWhite;
        }
    }
}
