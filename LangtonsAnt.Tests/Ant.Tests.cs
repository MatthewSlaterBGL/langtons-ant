using System.Collections.Generic;
using System.Drawing;
using LangtonsAnt.Classes;
using LangtonsAnt.Constants;
using NUnit.Framework;

namespace LangtonsAnt.Tests
{
    public class AntTests
    {
        private Ant _jimmy;

        [SetUp]
        public void Setup()
        {
            _jimmy = new Ant(Direction.North);
        }

        [Test]
        public void CheckAntsStartPositionIsValid()
        {
            Assert.AreEqual(new Point(0,0), _jimmy.Location);
        }

        [Test]
        public void CheckAntsStartDirectionIsValid()
        {
            Assert.AreEqual(Direction.North, _jimmy.CurrentDirection);
        }

        [Test]
        [TestCase(Direction.North, Direction.East)]
        [TestCase(Direction.East, Direction.South)]
        [TestCase(Direction.South, Direction.West)]
        [TestCase(Direction.West, Direction.North)]
        public void CheckAntCanTurnClockwise(Direction originalDirection, Direction expectedDirection)
        {
            var ant = new Ant(originalDirection);

            ant.TurnClockwise();

            Assert.AreEqual(expectedDirection, ant.CurrentDirection);
        }

        [Test]
        [TestCase(Direction.North, Direction.West)]
        [TestCase(Direction.East, Direction.North)]
        [TestCase(Direction.South, Direction.East)]
        [TestCase(Direction.West, Direction.South)]
        public void CheckAntCanTurnAntiClockwise(Direction originalDirection, Direction expectedDirection)
        {
            var ant = new Ant(originalDirection);

            ant.TurnAntiClockwise();

            Assert.AreEqual(expectedDirection, ant.CurrentDirection);
        }

        [Test]
        public void CheckAntMoves()
        {
            var jimmy = new Ant(Direction.East);

            jimmy.Move(); 

            Assert.AreNotEqual(new Point(0,0), jimmy.Location) ;
        }

        [Test]
        [TestCaseSource(nameof(MoveCases))]
        public void CheckAntMovesInDirectionItsFacing(Direction direction, Point expectedLocation)
        {
            var jimmy = new Ant(direction);

            jimmy.Move();

            Assert.AreEqual(expectedLocation, jimmy.Location);
        }

        [Test]
        public void CheckAntCanMoveMultipleTimes()
        {
            _jimmy.Move();
            _jimmy.TurnClockwise();
            _jimmy.Move();

            Assert.AreEqual(new Point(1,1), _jimmy.Location);
        }

        public static IEnumerable<TestCaseData> MoveCases
        {
            get
            {
                yield return new TestCaseData(Direction.North, new Point(0, 1));
                yield return new TestCaseData(Direction.East, new Point(1, 0));
                yield return new TestCaseData(Direction.South, new Point(0, -1));
                yield return new TestCaseData(Direction.West, new Point(-1, 0));
            }

        }
    }
}