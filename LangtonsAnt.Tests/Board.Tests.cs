﻿using System.Drawing;
using System.Linq;
using LangtonsAnt.Classes;
using NUnit.Framework;

namespace LangtonsAnt.Tests
{
    public class BoardTests
    {
        [Test]
        public void CheckBoardHasTenColumns()
        {
            var board = new Board();

            Assert.AreEqual(10, board.Squares.Count(square => square.Location.X == 0));
        }

        [Test]
        public void CheckEachRowHasTenSquares()
        {
            var board = new Board();

            Assert.AreEqual(100, board.Squares.Count);
        }

        [Test]
        public void CheckAllSquaresAreWhite()
        {
            var board = new Board();

            Assert.IsTrue(board.Squares.All(square => square.IsWhite));
        }

        [Test]
        public void CanChangeSquaresState()
        {
            var board = new Board();
            var point = new Point(0,0);
            var firstSquare = board.Squares.Single(square => square.Location == point);

            board.ToggleSquare(point);

            Assert.IsFalse(firstSquare.IsWhite);
        }

        [Test]
        public void CanToggleSquareState()
        {
            var board = new Board();
            var point = new Point(0, 0);

            board.ToggleSquare(point);
            board.ToggleSquare(point);

            var selectedSquare = board.Squares.Single(square => square.Location == point);

            Assert.IsTrue(selectedSquare.IsWhite);
        }
    }
}
